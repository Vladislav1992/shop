<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Интернет-магазин </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=153" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "/"> ГЛАВНАЯ </a></li>
	 <li> <a href = "#"> О НАС </a></li>
	 <li> <a href = "#"> КОНТАКТЫ </a></li>
	 <li> <a href = "#"> КАТАЛОГ </a>
	  <ul class = "hid"> 
<?php
    include('baza.php');
	$query = "SELECT * FROM shop_category";
	$result = mysqli_query($link, $query) or die(mysqli_error($link));
	for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
	foreach($data as $elem){
		echo "<li> <a href = \"goods.php?&url=$elem[id]&most=1\">$elem[title]</a></li>";
	}
	?>
	  </ul>
	 </li> 
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"profile.php\"> МОЙ ПРОФИЛЬ </a></li><li><a href = \"admin?&ask=1\">АДМИН</a></li><li><a href = \"admin/logout.php\"> ЛОГАУТ </a></li>";} else {echo "<li><a href = \"admin/login.php\"> АВТОРИЗАЦИЯ </a></li><li><a href = \"admin/adminLogin.php\"> АДМИН </a></li><li><a href = \"admin/register.php\"> РЕГИСТРАЦИЯ </a></li>";}
   if(isset( $_SESSION['prod'])){
   if(isset($_REQUEST['prod'])){
	   $try = $_REQUEST['prod'];
	   if(strpos($_SESSION['prod'], $try) !== 0){
	   if(!strpos($_SESSION['prod'], $try)){
	   $_SESSION['prod'] .= $try.' ';
	   }
	   }
   }
   }else {
      $_SESSION['prod'] = '';
   }	
   if(!empty($_SESSION['prod'])){
	   $pr = explode(' ', $_SESSION['prod']);
   array_pop($pr);
   }	
   //var_dump($_SESSION['prod']);
   //echo(strpos($_SESSION['prod'], $_REQUEST['prod']));
   //echo $_REQUEST['prod'];
   //var_dump($_POST['prod']);
?>	
     <li> <a href = "box.php"> <img class = "div" src = "images/div.png" alt = "корзина"> <? if(!empty($_SESSION['prod'])){if(count($pr) > 0){echo ' '.count($pr);}} ?></a></li>
	</ul>
   </header>
   <main>
    <h2> Рекомендации для Вас:</h2>
    <div class = "focus">
<?php
   //var_dump($_SESSION);
   //var_dump($_SESSION['prod']);
   //var_dump($pr);
   include('baza.php');
        
   $query = "SELECT * FROM `shop_goods` WHERE premium > 0 ORDER BY RAND() LIMIT 6";
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
   //var_dump($data);
   foreach($data as $elem){
	   //var_dump($elem);
   echo "<div class = \"focuses\">
	      <div class = \"picture\" style =\"background-image: url(images/$elem[id].jpg);\">
		   <a href=\"detail.php?&product=$elem[id]\"></a>
		  </div>
	      <div class = \"information\">
		    $elem[title]
			<br><br>
			$elem[price] р
		  </div>
		  <div class = \"box\">
		    <form method=\"POST\">
			 <input type=\"hidden\" name=\"prod\" value=\"$elem[id]\">
             <input type=\"submit\" name=\"in_box\" value=\"В корзину\">
            </form>
		  </div>	
	     </div> 
		  ";
   }
   //var_dump($_GET);
   //echo '<br>';
   //var_dump($_POST);
   //var_dump($_SESSION);
   //var_dump($pr);
?>
	</div>
	<h2> Горячие скидки ко Дню Защиты Комодских Варанов! Не пропусти интересное!</h2>
	<div class = "sales">
<?php
   include('baza.php');
        
   $query = "SELECT * FROM `shop_goods` WHERE sale > 0 ORDER BY RAND() LIMIT 6";
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
   //var_dump($data);
   foreach($data as $elem){
	   //var_dump($elem);
   echo "<div class = \"discount\">   
          <div class = \"picture_sale\" style =\"background-image: url(images/$elem[id].jpg);\">
		  <a href=\"detail.php?&product=$elem[id]\"></a>
	      </div> 
	      <div class = \"good_information\">
		    $elem[title]
		  </div>
		  <div class =\"sale\">
		   <p>
		    -$elem[sale]%
		   </p>	
		  </div>
		  <div class = \"information\">
			$elem[price] р
		  </div> 	
		  <div class = \"sale_box\">
		    <form method=\"POST\">
			 <input type=\"hidden\" name=\"prod\" value=\"$elem[id]\">
             <input type=\"submit\" name=\"in_box\" value=\"В корзину\">
            </form>
		  </div>	
	     </div> 
		  ";
   }   
   //var_dump($_GET);
   //echo '<br>';
   //var_dump($_POST);
   //var_dump($_SESSION);
   //$_SESSION['prod'] = null;  
?>	 
	</div>
   </main>
   <footer>
    <p><img src = "images/shop.png" alt = "shop"></p>
	<p>Copyright © 2001 - 2021  Shop.yes</p>
   </footer>
   </div>
 </body>   
</html>   